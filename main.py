import pandas as pd
import os
from itertools import combinations

file_groupes_sillons = 'groupes_sillons/VET_Info_semP25.xlsx'
directory_incomp_path = 'incomp_apogee'
files_incomp_apogee = [os.path.join(directory_incomp_path, file) for file in os.listdir(directory_incomp_path) if file.endswith('.xlsx')]

df = pd.read_excel(file_groupes_sillons)
df['Sillon'] = df['Sillon'].ffill()
df['CODE APOGEE'] = df['CODE APOGEE'].ffill()
df['INTITULE MATIERE'] = df['INTITULE MATIERE'].ffill()

sillon_to_groups_with_ue = {}
groupe_with_ue_to_sillons = {}
groupe_with_ue_to_ue = {}
groupe_with_ue_to_intitule = {}
groupe_with_ue_to_coll = {}
all_incompatibilities_apogee = set()
all_incompatibilities_theory = set()

# fonction pour charger les correspondances groupes/sillons
def dic_creator():
    current_sillon = None
    current_ue = None
    current_intitule = None
    for index, row in df.iterrows():
        group_name = row['Groupe'].strip().upper()
        sillon = row['Sillon']
        ue = row['CODE APOGEE'].strip().upper()
        intitule = row['INTITULE MATIERE']

        # si la cellule sillon est vide (cas fusion), on utilisera la dernière valeur connue
        if sillon:
            current_sillon = sillon
        # si la cellule ue est vide (cas fusion), on utilisera la dernière valeur connue
        if ue:
            current_ue = ue
        # si la cellule intitulé est vide (cas fusion), on utilisera la dernière valeur connue
        if intitule:
            current_intitule = intitule

        group_name_with_ue = current_ue + '-' + group_name
        sillons_list = str(current_sillon).split('+')

        for sillon in sillons_list:
            sillon = sillon.strip().upper()
            if sillon and any(sillon == f"{i}{letter}" for i in range(1, 9) for letter in ['', 'A', 'B']):
                if sillon in sillon_to_groups_with_ue:
                    sillon_to_groups_with_ue[sillon].append(group_name_with_ue)
                else:
                    sillon_to_groups_with_ue[sillon] = [group_name_with_ue]
            else:
                print(f"Sillon pas reconnu pour {group_name_with_ue}")
        groupe_with_ue_to_sillons[group_name_with_ue] = [s.strip().upper() for s in sillons_list]
        groupe_with_ue_to_ue[group_name_with_ue] = current_ue
        groupe_with_ue_to_intitule[group_name_with_ue] = current_intitule

def print_infos():
    print("--- Liste des groupes pour chaque sillon :")
    for sillon, groupes in sillon_to_groups_with_ue.items():
        print(f"Sillon {sillon}: {groupes}")
    print("\n--- Sillons pour chaque groupe :")
    for groupe, sillons in groupe_with_ue_to_sillons.items():
        print(f"Groupe {groupe}: {sillons}")
    print("\n--- UE pour chaque groupe :")
    for groupe, cours in groupe_with_ue_to_ue.items():
        print(f"Groupe {groupe}: {cours}, {groupe_with_ue_to_intitule[groupe]}")
    print(f"Nombre de groupes  : {len(groupe_with_ue_to_ue)}")

# fonction pour vérifier si deux groupes donnés sont incompatibles (même sillon
# en tenant compte des demi-sillons)
def check_incompatibility(group1, group2, sillon_dict):
    group1_sillons = set(sillon for sillon, groups in sillon_dict.items() if group1 in groups)
    group2_sillons = set(sillon for sillon, groups in sillon_dict.items() if group2 in groups)

    for sillon1 in group1_sillons:
        for sillon2 in group2_sillons:
            if sillon1 == sillon2:
                return True, group1_sillons, group2_sillons
            elif ''.join(filter(str.isdigit, sillon1)) == ''.join(filter(str.isdigit, sillon2)):
                if not ((sillon1.endswith('A') and sillon2.endswith('B')) or (
                        sillon1.endswith('B') and sillon2.endswith('A'))):
                    return True, group1_sillons, group2_sillons
    return False, group1_sillons, group2_sillons

# fonction qui vérifie si une incomp enregistrée dans Apogee renseigne 2 groupes qui appartiennent à
# des sillons incompatibles. Si les groupes sont "en théorie" compatibles alors un message d'erreur
# est affiché.
def verifier_apogee_vs_theory():
    for file in files_incomp_apogee:
        sheets = pd.read_excel(file, sheet_name=None, engine='openpyxl')  # Charger tous les onglets
        for sheet_name, df in sheets.items():  # Parcourir chaque onglet
            print(f"Traitement de l'onglet : {sheet_name}")
            for index, row in df.iterrows():
                group = row['COD_EXT_GPE'].upper()
                group1 = row['COD_EXT_GPE1'].upper()
                ue = row['COD_EXT_COL'].upper()[:8]
                ue1 = row['COD_EXT_COL1'].upper()[:8]

                group_with_ue = ue + '-' + group
                group1_with_ue1 = ue1 + '-' + group1

                groupe_with_ue_to_coll[group_with_ue] = row['COD_EXT_COL'].upper()
                groupe_with_ue_to_coll[group1_with_ue1] = row['COD_EXT_COL1'].upper()

                # on vérifie que les 2 groupes de l'incomp appartiennent à la VET
                if (group_with_ue in groupe_with_ue_to_sillons and groupe_with_ue_to_sillons[group_with_ue]
                        and group1_with_ue1 in groupe_with_ue_to_sillons and groupe_with_ue_to_sillons[group1_with_ue1]):
                    # normalisation de l'ordre pour garantir la cohérence
                    group_pair = tuple(sorted((group_with_ue, group1_with_ue1)))

                    incompatibility, g_sillons, g1_sillons = check_incompatibility(group_with_ue, group1_with_ue1,
                                                                                sillon_to_groups_with_ue)
                    if incompatibility:
                        all_incompatibilities_apogee.add(group_pair)
                    else:
                        print(f"Erreur trouvée: {group} ({groupe_with_ue_to_ue[group_with_ue]}, {groupe_with_ue_to_intitule[group_with_ue]}) et "
                              f"{group1} ({groupe_with_ue_to_ue[group1_with_ue1]}, {groupe_with_ue_to_intitule[group1_with_ue1]}) "
                            f"ne sont pas sur le même sillon mais déclarés incompatibles dans Apogee. "
                            f"Sillon(s) de {group}: {g_sillons}, Sillon(s) de {group1}: {g1_sillons}")

# fonction qui génére toutes les incomp d'après la théorie de l'association groupes/sillons
def incomp_theory_creator():
    for sillon, groups in sillon_to_groups_with_ue.items():
        # ajout de toutes les paires du même sillon
        for group1, group2 in combinations(groups, 2):
            if (groupe_with_ue_to_ue[group1] != groupe_with_ue_to_ue[group2]): # les groupes doivent appartenir à des UE différentes
                all_incompatibilities_theory.add(tuple(sorted((group1, group2))))
        # ajout des paires entre demi-sillons A et non-A ou B et non-B
        base_sillon = ''.join(filter(str.isdigit, sillon))
        for other_sillon, other_groups in sillon_to_groups_with_ue.items():
            if other_sillon != sillon and ''.join(filter(str.isdigit, other_sillon)) == base_sillon:
                if not ((sillon.endswith('A') and other_sillon.endswith('B')) or (sillon.endswith('B') and other_sillon.endswith('A'))):
                    for group1 in groups:
                        for group2 in other_groups:
                            all_incompatibilities_theory.add(tuple(sorted((group1, group2))))

# fonction qui vérifie si une incomp qui devrait exister en théorie est bien enregistrée dans Apogee.
# Si ce n'est pas le cas alors un message d'erreur est affiché.
def verifier_theory_vs_apogee():
    missing_incompatibilities = all_incompatibilities_theory - all_incompatibilities_apogee
    if missing_incompatibilities:
        print("Oublis détectés dans les incompatibilités:")
        for group1, group2 in missing_incompatibilities:
            print(f"{group1.split('-')[0]} ({groupe_with_ue_to_ue[group1]}, {groupe_with_ue_to_intitule[group1]}) et "
                  f"{group2.split('-')[0]} ({groupe_with_ue_to_ue[group2]}, {groupe_with_ue_to_intitule[group2]}) "
                  f"devraient être incompatibles mais ne sont pas listés dans Apogée.")
    else:
        print("Aucun oubli détecté.")


# vérifie que tous les groupes présents comme clés dans le dictionnaire `groupe_to_ue`
# apparaissent dans au moins une paire de `all_incompatibilities_apogee`.
def verify_group_names_are_in_apogee():
    missing_groups = set()

    for groupe in groupe_with_ue_to_ue.keys():
        # vérifie si le groupe est dans au moins une paire d'incompatibilités
        if not any(groupe in pair for pair in all_incompatibilities_apogee):
            missing_groups.add(groupe)
    if missing_groups:
        print(f"Les groupes suivants ne sont pas référencés dans Apogée : {missing_groups}")
    else:
        print("Tous les groupes souhaités sont référencés dans Apogée ")


def export_missing_incomp():
    missing_incompatibilities = all_incompatibilities_theory - all_incompatibilities_apogee
    if missing_incompatibilities:
        data = []
        for group1, group2 in missing_incompatibilities:
            # Vérification de la présence des collections
            coll1 = groupe_with_ue_to_coll.get(group1, "???")
            coll2 = groupe_with_ue_to_coll.get(group2, "???")

            # Afficher un message si une collection est inconnue
            if coll1 == "???":
                print(f"Attention, collection inconnue pour le groupe {group1}")
            if coll2 == "???":
                print(f"Attention, collection inconnue pour le groupe {group2}")

            data.append([
                group1.split('-')[0],
                groupe_with_ue_to_ue[group1],
                groupe_with_ue_to_intitule[group1],
                coll1,
                group2.split('-')[0],
                groupe_with_ue_to_ue[group2],
                groupe_with_ue_to_intitule[group2],
                coll2
            ])

        df = pd.DataFrame(data, columns=[
            'Groupe 1', 'UE 1', 'Intitule 1', 'Coll 1',
            'Groupe 2', 'UE 2', 'Intitule 2', 'Coll 2'
        ])

        df.to_excel('incompatibilites_manquantes.xlsx', index=False)
        print("Oublis détectés dans les incompatibilités et écrits dans 'incompatibilites_manquantes.xlsx'.")
    else:
        print("Aucun oubli détecté.")


dic_creator()
print_infos()
print("--- Apogee vs. theory:")
verifier_apogee_vs_theory()
print("--- Theory vs. apogee:")
incomp_theory_creator()
verify_group_names_are_in_apogee()
print(f"Theory nb incomp : {len(all_incompatibilities_theory)}")
print(f"Apogee nb incomp : {len(all_incompatibilities_apogee)}")
#verifier_theory_vs_apogee()
export_missing_incomp()


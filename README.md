# Incomp Verifierz

Script Python pour vérifier les incompatibilités de groupes enregistrées dans Apogée, spécifiquement conçu pour gérer les subtilités des demi-sillons A et B.

## Données de Départ

- **Fichier de Groupes et Sillons** : un fichier Excel spécifiant chaque groupe et ses sillons associés. Chaque ligne doit contenir un groupe et son sillon, où les sillons peuvent être notés comme '1', '1A', ou '1B'.
- **Exports des Incompatibilités Apogée** : plusieurs fichiers Excel listant les incompatibilités enregistrées. Chaque fichier doit avoir deux colonnes indiquant les groupes qui sont incompatibles.

## Fonctionnalités

### Apogée vs. Théorie

Vérifie que chaque paire d'incompatibilité enregistrée correspond à des groupes qui partagent le même sillon, incluant les distinctions entre les sillons principaux et leurs subdivisions A et B.

### Théorie vs. Apogée

Assure que toutes les incompatibilités théoriques, basées sur les associations de sillons, sont correctement enregistrées dans Apogée.
